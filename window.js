
var axios = require('axios').default;
var url = require("URL")
var moment = require("moment-twitter")
var temperature = document.getElementById("currentTemp")
var hum = document.getElementById("humidity")
var precipProb = document.getElementById("precip")
var currentUVIndex = document.getElementById("UV")
var currentTime = document.getElementById("time")
var imgDiv = document.getElementById("imgDiv")
var icon = document.createElement("img")
var parent = document.getElementById("weather")
var zip = 60181
var apiKey = "134fa330d08f5b618801a6aaa8bffabb"




function checkWeather(firstrun){
axios.get("https://public.opendatasoft.com/api/records/1.0/search/?dataset=us-zip-code-latitude-and-longitude&q="+zip+"&facet=state&facet=timezone&facet=dst").then(function(response){
    longitude = response.data["records"][0]["fields"]["geopoint"][0]
    latitude = response.data["records"][0]["fields"]["geopoint"][1]
    apiURL = "https://api.darksky.net/forecast/"+apiKey+"/"+longitude+","+latitude
    //console.log(apiURL)

    axios.get(apiURL, firstrun).then(function(response){
        var weatherStatus = response.data
        var temp = weatherStatus["currently"]["temperature"]
        var humid = weatherStatus["currently"]["humidity"]
        var UV = weatherStatus["currently"]["uvIndex"]
        var precipProbability = weatherStatus["currently"]["precipProbability"]
        var time = moment(weatherStatus["currently"]["time"]*1000).twitterShort()
        temperature.innerHTML = temp;
        hum.innerHTML = humid;
        precipProb.innerHTML = precipProbability;
        currentTime.innerHTML = time;
        currentUVIndex.innerHTML = UV;
        //console.log(firstrun)
        switch (weatherStatus["minutely"]["icon"]){
            case "partly-cloudy-day":
                iconSrc = "./weather-icons-master/svg/wi-day-cloudy.svg"
                break;
            case "clear-day":
                iconSrc = "./weather-icons-master/svg/wi-day-sunny.svg"
                break;
            case "clear-night":
                iconSrc = "./weather-icons-master/svg/wi-night-clear.svg"
                break;
            case "partly-cloudy-night":
                iconSrc = "./weather-icons-master/svg/wi-night-cloudy.svg"
                break;
            case "cloudy":
                iconSrc = "./weather-icons-master/svg/wi-cloud.svg"
                break;
            case "rain":
                iconSrc = "./weather-icons-master/svg/wi-rain.svg"
                break;
            case "sleet":
                iconSrc = "./weather-icons-master/svg/wi-sleet.svg"
                break;
            case "snow":
                iconSrc = "./weather-icons-master/svg/wi-snow.svg"
                break;
            case "fog":
                iconSrc = "./weather-icons-master/svg/wi-fog.svg"
                break;
            }
        icon.src=iconSrc
        if (firstrun == true) {
            parent.replaceChild(icon, imgDiv)
        }
        })
}
)
}



checkWeather(true);

setInterval(function() {
    checkWeather(false);
    //console.log("doing things")
}, 60000);

